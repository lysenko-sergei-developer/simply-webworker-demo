function changeBackground(e) {
  let changeBackground = document.getElementById("background-btn");
  
  let bodyBackground = document.getElementsByTagName("body")[0];
  bodyBackground.style.background = generateColor();
}

function generateColor() {
  var text = "";
  var possible = "abcdef0123456789";

  for (var i = 0; i < 6; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return "#"+text;
}

function handleHardSort() {
    if(window.Worker) {
    let worker = new Worker(URL.createObjectURL(new Blob(["("+worker_function.toString()+")()"], {type: 'text/javascript'})));
    let calculateRes = document.getElementById("calculate-res");
    
    worker.postMessage(null);
    worker.onmessage = function(e) {
      calculateRes.innerHTML = e.data

      setTimeout((function() {
        calculateRes.innerHTML = "No calculate";
      }), 1000);
    };
   }
  function worker_function() {
    onmessage = function(e) {
      if(e.data !== undefined) {
        let arr = [0, 1, 3, 4, 5, 6, 9, 1, 4, 3, 0, 4, 6];

        for (let i = 0; i < 10000000; i++) {
          shuffle(arr);
        }

        postMessage("Calculate succesful!");
      }
    }

    function shuffle(a) {
      var j, x, i;
      for (i = a.length; i; i--) {
          j = Math.floor(Math.random() * i);
          x = a[i - 1];
          a[i - 1] = a[j];
          a[j] = x;
      }
    }
  }
}