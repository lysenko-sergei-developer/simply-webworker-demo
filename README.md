# simply-webworker-demo
This project demonstrate powerful of WebWorkers HTML5

[DEMO without workers](https://github.com/lysenko-sergei-developer/simply-webworker-demo/blob/without-webworkers/index.html)

[DEMO with workers](https://github.com/lysenko-sergei-developer/simply-webworker-demo/blob/master/index.html)
